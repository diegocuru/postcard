//
//  PostcardTableViewCell.swift
//  Postcard
//
//  Created by Diego Curumaco on 5/05/21.
//

import UIKit

class PostcardTableViewCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var postcardImageView: UIImageView!
    @IBOutlet weak var styleTitleLabel: UILabel!
    @IBOutlet weak var styleLabel: UILabel!
    @IBOutlet weak var frontTextTitleLabel: UILabel!
    @IBOutlet weak var frontTextLabel: UILabel!
    @IBOutlet weak var backTextStackView: UIStackView!
    @IBOutlet weak var backTextTitleLabel: UILabel!
    @IBOutlet weak var backTextLabel: UILabel!
    // MARK: - Vars
    var postcard: Postcard?

    // MARK: - Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setVisualAdjustments()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
// MARK: Utitlities
extension PostcardTableViewCell {
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle.main)
    }
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
// MARK: Visual adjustments and Information
extension PostcardTableViewCell {
    private func setVisualAdjustments() {
        self.postcardImageView.layer.cornerRadius = 10.0
        self.postcardImageView.clipsToBounds = true
    }
    
    func updatePostcardInfo() {
        guard let postcard = self.postcard else { return }
        
        self.styleLabel.text = postcard.style.name
        self.frontTextLabel.text = postcard.frontText
        self.backTextStackView.isHidden = postcard.backText == nil
        
        if let backText = postcard.backText {
            self.backTextLabel.text = backText
        }
        
        guard let imagePath = postcard.imagePath else { return }
        guard let image = ImageRepository.loadImage(from: imagePath) else { return }
        self.postcardImageView.image = image
    }
}

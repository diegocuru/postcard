//
//  ArchiveViewController.swift
//  Postcard
//
//  Created by Diego Curumaco on 2/05/21.
//

import UIKit

class ArchiveViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var styleSegmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Variables
    var postcards: [Postcard]?
    var filteredPostcards: [Postcard]?

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addDelegateAndDataSource()
        self.registerTableViewCell()
        self.setVisualAdjustments()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadPostcardsToShow()
    }
}
// MARK: - View Setup
extension ArchiveViewController {
    private func registerTableViewCell() {
        let nib = PostcardTableViewCell.nib
        let reuseIdentifier = PostcardTableViewCell.reuseIdentifier
        self.tableView.register(nib, forCellReuseIdentifier: reuseIdentifier)
    }
    
    private func setVisualAdjustments() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    private func addDelegateAndDataSource() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
}
// MARK: - Navigation
extension ArchiveViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ArchiveToNew" {
            if let postcard = sender as? Postcard {
                if let tabBar = segue.destination as? PostcardCreationTabBarController {
                    tabBar.postcardToEdit = postcard
                }
            }
        }
    }
}
// MARK: Table Datasource
extension ArchiveViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let postcards = self.getPostcardListBy(segmentedControl: self.styleSegmentedControl) else {
            return 0
        }
        
        return postcards.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let postcards = self.getPostcardListBy(segmentedControl: self.styleSegmentedControl) else {
            return UITableViewCell(style: .default, reuseIdentifier: "blank")
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: PostcardTableViewCell.reuseIdentifier) as! PostcardTableViewCell
        let postcard = postcards[indexPath.row]
        cell.postcard = postcard
        cell.updatePostcardInfo()
        return cell
    }
    
    private func getPostcardListBy(segmentedControl: UISegmentedControl) -> [Postcard]? {
        return segmentedControl.selectedSegmentIndex == 0 ? self.postcards : self.filteredPostcards
    }
}
// MARK: - Table Delegate
extension ArchiveViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let postcards = self.postcards else { return }
        
        let postcard = postcards[indexPath.row]
        self.showAlertActionForPostCardSelected(postcard, at: indexPath)
    }
}
// MARK: - UI Interaction
extension ArchiveViewController {
    @IBAction func styleSegmentedHandler(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 1:
            self.filterPostcardsBy(style: .regular)
        case 2:
            self.filterPostcardsBy(style: .foldable)
        default:
            self.removeFilter()
        }
    }
}
// MARK: - Data Interaction
extension ArchiveViewController {
    private func loadPostcardsToShow() {
        self.postcards = PostcardRepository.getStoredPostcards()
        self.tableView.reloadData()
    }
    
    private func showAlertActionForPostCardSelected(_ postcardSelected: Postcard, at indexPath: IndexPath) {
 
        let alertViewController = UIAlertController(title: "Actions for Postcard", message: "Choose some action to do", preferredStyle: .actionSheet)
 
        let loadAction = UIAlertAction(title: "Load", style: .default) { _ in
            self.performSegue(withIdentifier: "ArchiveToNew", sender: postcardSelected)
        }
        
        let deleteAction = UIAlertAction(title: "Delete", style: .default) { _ in
            PostcardRepository.delete(postcard: postcardSelected) { [weak self] deleted in
                if deleted {
                    self?.postcards?.remove(at: indexPath.row)
                    self?.tableView.deleteRows(at: [indexPath], with: .fade)
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertViewController.addAction(loadAction)
        alertViewController.addAction(deleteAction)
        alertViewController.addAction(cancelAction)
        alertViewController.view.layoutIfNeeded()
        self.present(alertViewController, animated: true, completion: nil)
    }
    
    private func filterPostcardsBy(style: PostcardStyle) {
        guard let postcards = self.postcards else { return }
        self.filteredPostcards = postcards.filter({ $0.style == style })
        self.tableView.reloadData()
    }
    
    private func removeFilter() {
        self.filteredPostcards = nil
        self.tableView.reloadData()
    }
}

//
//  PostcardCreationTabBarController.swift
//  Postcard
//
//  Created by Diego Curumaco on 30/04/21.
//

import UIKit

class PostcardCreationTabBarController: UITabBarController {
    
    var postcardToEdit: Postcard?
    var style: PostcardStyle = .regular
    var image: UIImage?
    var frontText: String?
    var backText: String?
    var preview: UIImage?
    var isFirstLoad: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addDelegates()
        self.setVisualAdjustments()
        if let postcardToEdit = self.postcardToEdit { self.prepareForEdition(with: postcardToEdit) }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.updateTabBarItems()
        self.updateViewControllers()
        self.validateFirstLoadForEdition()
        super.viewWillAppear(animated)
    }
}

extension PostcardCreationTabBarController {
    private func setVisualAdjustments() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    private func addDelegates() {
        if let viewControllers = self.viewControllers {
            if let postcardStyleVC = viewControllers[0] as? PostcardStyleViewController {
                postcardStyleVC.delegate = self
            }
            if let postcardImageVC = viewControllers[1] as? PostcardImageViewController {
                postcardImageVC.delegate = self
            }
            if let postcardTextVC = viewControllers[2] as? PostcardTextViewController {
                postcardTextVC.delegate = self
            }
            if let postcardPreviewVC = viewControllers[3] as? PostcardPreviewViewController {
                postcardPreviewVC.delegate = self
            }
            if let summaryVC = viewControllers[4] as? SummaryViewController {
                summaryVC.delegate = self
            }
        }
    }
}

extension PostcardCreationTabBarController {
    private func setTabBarItemTo(enabled: Bool, atPosition index: Int) {
        guard let items = self.tabBar.items else {
            return
        }
        
        if index > items.count {
            return
        }
        
        items[index].isEnabled = enabled
    }
    
    private func prepareForEdition(with postcard: Postcard) {
        self.style = postcard.style
        self.frontText = postcard.frontText
        self.backText = postcard.backText
        if let imagePath = postcard.imagePath {
            if let image = ImageRepository.loadImage(from: imagePath) { self.image = image }
        }
        if let previewPath = postcard.previewPath {
            if let preview = ImageRepository.loadImage(from: previewPath) { self.preview = preview }
        }
    }
    
    private func updateTabBarItems() {
        self.setTabBarItemTo(enabled: true, atPosition: 0) // style
        self.setTabBarItemTo(enabled: true, atPosition: 1) // image
        self.setTabBarItemTo(enabled: self.image != nil, atPosition: 2) // text
        self.setTabBarItemTo(enabled: self.frontText != nil, atPosition: 3) // preview
        self.setTabBarItemTo(enabled: self.preview != nil, atPosition: 4) // summary
    }
    
    private func updateViewControllers() {
        guard let viewControllers = self.viewControllers else { return }
        
        for viewController in viewControllers {
            if let styleViewController = viewController as? PostcardStyleViewController {
                styleViewController.style = self.style
            } else if let textViewController = viewController as? PostcardTextViewController {
                textViewController.postcardStyle = self.style
                textViewController.frontText = self.frontText
                textViewController.backText = self.backText
            } else if let previewViewController = viewController as? PostcardPreviewViewController {
                previewViewController.style = self.style
                previewViewController.frontText = self.frontText
                previewViewController.backText = self.backText
                previewViewController.image = self.image
            } else if let imageViewController = viewController as? PostcardImageViewController {
                imageViewController.postcardImage = self.image
            } else if let summaryViewController = viewController as? SummaryViewController {
                summaryViewController.image = self.image
                summaryViewController.preview = self.preview
                do {
                    let id = self.postcardToEdit?.id ?? UUID()
                    let postcard = try Postcard(id: id,
                                                style: self.style,
                                                frontText: self.frontText ?? "",
                                                backText: self.backText,
                                                imagePath: nil,
                                                lastModified: Date(),
                                                previewPath: nil)
                    summaryViewController.postcard = postcard
                } catch {
                    print("Could not create postcard")
                }
            }
        }
    }
    
    private func validateFirstLoadForEdition() {
        if self.postcardToEdit != nil {
            self.selectedIndex = 3
        }
        self.isFirstLoad = false
    }
}

extension PostcardCreationTabBarController: PostcardStyleDelegate {
    func styleSelected(_ style: PostcardStyle) {
        self.style = style
        self.updateTabBarItems()
        self.updateViewControllers()
    }
}

extension PostcardCreationTabBarController: PostcardImageDelegate {
    func imageSelected(_ image: UIImage) {
        self.image = image
        self.updateTabBarItems()
        self.updateViewControllers()
    }
}

extension PostcardCreationTabBarController: PostcardTextDelegate {
    func textChanged(frontText: String?, backText: String?) {
        self.frontText = frontText
        self.backText = backText
        self.updateTabBarItems()
        self.updateViewControllers()
    }
}

extension PostcardCreationTabBarController: PostcardPreviewDelegate {
    func postcardPreviewCreated(_ preview: UIImage) {
        self.preview = preview
        self.updateTabBarItems()
        self.updateViewControllers()
    }
}

extension PostcardCreationTabBarController: SummaryDelegate {
    func postcardSaved() {
        self.navigationController?.popViewController(animated: true)
    }
}

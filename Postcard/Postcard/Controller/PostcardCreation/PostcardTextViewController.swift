//
//  PostcardTextViewController.swift
//  Postcard
//
//  Created by Diego Curumaco on 30/04/21.
//

import UIKit

protocol PostcardTextDelegate {
    func textChanged(frontText: String?, backText: String?)
}

class PostcardTextViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var frontTextTitleLavel: UILabel!
    @IBOutlet weak var charactersCountLabel: UILabel!
    @IBOutlet weak var frontTextView: UITextView!
    @IBOutlet weak var backTextTitleLabel: UILabel!
    @IBOutlet weak var wordsCountLabel: UILabel!
    @IBOutlet weak var backTextView: UITextView!
    
    
    var postcardStyle: PostcardStyle = .regular
    var delegate: PostcardTextDelegate?
    var frontText: String?
    var backText: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setVisualAdjustments()
        self.addDelegates()
        self.addGestures()
        self.addKeyboardObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUIFor(postcardStyle: self.postcardStyle)
    }
}

extension PostcardTextViewController {
    private func setVisualAdjustments() {
        self.frontTextView.layer.borderWidth = 1.0
        self.frontTextView.layer.borderColor = UIColor.tertiaryLabel.cgColor
        self.frontTextView.layer.cornerRadius = 8.0
        
        self.backTextView.layer.borderWidth = 1.0
        self.backTextView.layer.borderColor = UIColor.tertiaryLabel.cgColor
        self.backTextView.layer.cornerRadius = 8.0
        
        if let frontText = self.frontText {
            self.frontTextView.text = frontText
        }
        if let backText = self.backText {
            self.backTextView.text = backText
        }
    }
    
    private func addDelegates() {
        self.frontTextView.delegate = self
        self.backTextView.delegate = self
    }
    
    private func addKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func addGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(backgroundViewTapped))
        self.scrollView.addGestureRecognizer(tapGesture)
    }
}

extension PostcardTextViewController {
    private func setUIFor(postcardStyle: PostcardStyle) {
        self.backTextTitleLabel.isHidden = postcardStyle == .regular
        self.wordsCountLabel.isHidden = postcardStyle == .regular
        self.backTextView.isHidden = postcardStyle == .regular
    }
}

extension PostcardTextViewController {
    @objc func keyboardWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        guard let keyboardRect = userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? CGRect  else {
            return
        }
        self.scrollView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardRect.height, right: 0.0)
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification) {
        self.scrollView.contentInset = UIEdgeInsets.zero
    }
    
    @objc func backgroundViewTapped() {
        self.view.endEditing(true)
    }
}

extension PostcardTextViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        if textView == self.frontTextView {
            return TextValidator.isValidFrontText(newText, charsLimit: 30)
        } else if textView == self.backTextView {
            return TextValidator.isValidBackText(newText, wordsLimit: 30)
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView == self.frontTextView {
            self.charactersCountLabel.text = "Characters count: \(textView.getCharactersCount())"
        } else if textView == self.backTextView {
            if !self.wordsCountLabel.isHidden {
                self.wordsCountLabel.text = "Words count: \(textView.getWordsCount())"
            }
        }
        let frontText = self.frontTextView.text
        let backText = self.postcardStyle == .foldable ? self.backTextView.text : nil
        self.delegate?.textChanged(frontText: frontText, backText: backText)
    }
}

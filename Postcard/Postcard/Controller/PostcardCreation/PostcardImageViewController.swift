//
//  PostcardImageViewController.swift
//  Postcard
//
//  Created by Diego Curumaco on 30/04/21.
//

import UIKit

protocol PostcardImageDelegate {
    func imageSelected(_ image: UIImage)
}

class PostcardImageViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    private var imagePickerController: UIImagePickerController?
    var delegate: PostcardImageDelegate?
    var postcardImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setImagePickerController()
        self.setVisualAdjustments()
    }
    
}

extension PostcardImageViewController {
    private func setImagePickerController() {
        self.imagePickerController = UIImagePickerController()
        self.imagePickerController?.delegate = self
        self.imagePickerController?.allowsEditing = true
        self.imagePickerController?.mediaTypes = ["public.image"]
        self.imagePickerController?.sourceType = .camera
    }
    
    private func setVisualAdjustments() {
        self.imageView.layer.cornerRadius = 10.0
        self.imageView.clipsToBounds = true
        
        if let image = self.postcardImage {
            self.imageView.image = image
        }
    }
}

extension PostcardImageViewController {
    @IBAction func imageFromCamera(_ sender: UIButton) {
        guard let imagePicker = self.imagePickerController else { return }
        imagePicker.sourceType = .camera
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func imageFromGallery(_ sender: UIButton) {
        guard let imagePicker = self.imagePickerController else { return }
        imagePicker.sourceType = .photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
}

extension PostcardImageViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else { return }
        self.imageView.image = image
        self.delegate?.imageSelected(image)
        dismiss(animated: true)
    }
    
    
}

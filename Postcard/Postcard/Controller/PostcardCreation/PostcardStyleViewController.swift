//
//  PostcardStyleViewController.swift
//  Postcard
//
//  Created by Diego Curumaco on 30/04/21.
//

import UIKit

protocol PostcardStyleDelegate {
    func styleSelected(_ style: PostcardStyle)
}

class PostcardStyleViewController: UIViewController {

    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    var delegate: PostcardStyleDelegate?
    var style: PostcardStyle = .regular
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setVisualAdjustments()
    }
}

extension PostcardStyleViewController {
    private func setVisualAdjustments() {
        self.segmentedControl.selectedSegmentIndex = style == .regular ? 0 : 1
    }
}

extension PostcardStyleViewController {
    @IBAction func styleSelected(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            self.delegate?.styleSelected(.regular)
        case 1:
            self.delegate?.styleSelected(.foldable)
        default:
            print("Option not supported")
        }
    }
}

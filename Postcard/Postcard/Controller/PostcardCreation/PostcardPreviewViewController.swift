//
//  PostcardPreviewViewController.swift
//  Postcard
//
//  Created by Diego Curumaco on 30/04/21.
//

import UIKit

protocol PostcardPreviewDelegate {
    func postcardPreviewCreated(_ preview: UIImage)
}

class PostcardPreviewViewController: UIViewController {

    @IBOutlet weak var postcardPreviewStackView: UIStackView!
    @IBOutlet weak var frontContainerView: UIView!
    @IBOutlet weak var backContainerView: UIView!
    @IBOutlet weak var frontView: UIView!
    @IBOutlet weak var frontTextLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var backTextLabel: UILabel!

    var delegate: PostcardPreviewDelegate?
    var style: PostcardStyle = .regular
    var frontText: String?
    var backText: String?
    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setVisualAdjustments()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updatePreview()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.layoutIfNeeded()
        self.prepareToSavePostcard()
    }
}

extension PostcardPreviewViewController {
    private func setVisualAdjustments() {
        self.postcardPreviewStackView.layer.borderWidth = 1.0
        self.postcardPreviewStackView.layer.borderColor = UIColor.secondaryLabel.cgColor
        self.imageView.layer.cornerRadius = 10.0
        self.imageView.clipsToBounds = true
    }
    
    private func updatePreview() {
        self.backContainerView.isHidden = self.style == .regular
        
        if let frontText = self.frontText {
            self.frontTextLabel.text = frontText
        }
        if let image = self.image {
            self.imageView.image = image
        }
        self.backTextLabel.text = self.backText ?? ""
    }
    
    private func prepareToSavePostcard() {
        let renderer = UIGraphicsImageRenderer(size: self.postcardPreviewStackView.bounds.size)
        let preview = renderer.image { context in
            self.postcardPreviewStackView.drawHierarchy(in: self.postcardPreviewStackView.bounds, afterScreenUpdates: true)
        }
        self.delegate?.postcardPreviewCreated(preview)
    }
}

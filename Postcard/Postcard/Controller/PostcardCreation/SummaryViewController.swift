//
//  SummaryViewController.swift
//  Postcard
//
//  Created by Diego Curumaco on 30/04/21.
//

import UIKit

enum SummaryViewState {
    case normal
    case saving
    case saved
    case error
}

protocol SummaryDelegate {
    func postcardSaved()
}

class SummaryViewController: UIViewController {

    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    var delegate: SummaryDelegate?
    var postcard: Postcard?
    var image: UIImage?
    var preview: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUITo(state: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updatePostcardInfo()
    }
}

// MARK: - View Controller Setup
extension SummaryViewController {
    private func updatePostcardInfo() {
        guard let postcard = self.postcard else {
            return
        }
        var summary = "Postcard of style: \(postcard.style.name)"
        summary.append("\nFront Text: \(postcard.frontText)")
        if let backText = postcard.backText {
            summary.append("\nBack Text: \(backText)")
        }
        self.summaryLabel.text = summary
    }
}

// MARK: - UI State Control
extension SummaryViewController {
    private func setUITo(state: SummaryViewState) {
        func setSummaryActivityIndicatorHidden(hidden: Bool, animated: Bool) {
            func setActivityIndicatorHidden(hidden: Bool) {
                self.saveButton.isEnabled = hidden
            }
            animated ?
                UIView.animate(withDuration: 1) {setActivityIndicatorHidden(hidden: hidden)} : setActivityIndicatorHidden(hidden: hidden)
            
            self.saveButton.setTitle(hidden ? "Save Postcard" : "", for: .disabled)
            self.saveButton.setTitle(hidden ? "Save Postcard" : "", for: .normal)
            self.activityIndicatorView.isHidden = hidden
            
            hidden ?
                self.activityIndicatorView.stopAnimating() :
                self.activityIndicatorView.startAnimating()
        }
        
        func setUIToNormal() {
            self.errorLabel.text = ""
            setSummaryActivityIndicatorHidden(hidden: true, animated: true)
        }
        
        func setUIToSaving() {
            self.errorLabel.text = ""
            setSummaryActivityIndicatorHidden(hidden: false, animated: true)
        }
        
        func setUIToSaved() {
            self.errorLabel.text = "Postcard saved"
            setSummaryActivityIndicatorHidden(hidden: true, animated: true)
        }
        
        func setUIToError() {
            self.errorLabel.text = "Some error ocurred while saving postcard"
            setSummaryActivityIndicatorHidden(hidden: true, animated: true)
        }
        
        switch state {
        case .normal:
            setUIToNormal()
        case .saving:
            setUIToSaving()
        case .saved:
            setUIToSaved()
        case .error:
            setUIToError()
        }
    }
    
    private func handleTransactionResult(with success: Bool) {
        if success {
            self.setUITo(state: .normal)
            self.delegate?.postcardSaved()
        } else {
            self.setUITo(state: .error)
        }
    }
}

// MARK: - UI Interaction Control
extension SummaryViewController {
    @IBAction func save(_ sender: UIButton) {
        guard let postcard = self.postcard else { return }
        
        self.setUITo(state: .saving)
        
        guard let image = self.image, let preview = self.preview else {
            self.setUITo(state: .error)
            return
        }
        
        if PostcardRepository.postCardExists(id: postcard.id.uuidString) {
            PostcardRepository.update(postcard: postcard, image: image, preview: preview) { updated in
                self.handleTransactionResult(with: updated)
            }
        } else {
            PostcardRepository.save(postcard: postcard, image: image, preview: preview) { saved in
                self.handleTransactionResult(with: saved)
            }
        }
    }
}

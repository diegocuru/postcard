//
//  LoginViewController.swift
//  Postcard
//
//  Created by Diego Curumaco on 27/04/21.
//

import UIKit

enum LoginViewState {
    case normal
    case login
    case showingMessage
}

enum LoginErrorType {
    case usernameIsMissing
    case passwordIsMissing
    case loginRequest
    
    var text: String {
        switch self {
        case .usernameIsMissing:
            return "User name is missing"
        case .passwordIsMissing:
            return "Password is missing"
        case .loginRequest:
            return "Error while doing Sign In"
        }
    }
}

class LoginViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewTitleLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var errorDescriptionLabel: UILabel!
    @IBOutlet weak var loginActivityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addKeyboardObservers()
        self.addGestures()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUIFor(state: .normal, errorDescription: nil)
    }
}

// MARK: - View Controller Setup
extension LoginViewController {
    private func addKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func addGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(backgroundViewTapped))
        self.scrollView.addGestureRecognizer(tapGesture)
    }
}

// MARK: - UI State Control
extension LoginViewController {
    private func setUIFor(state: LoginViewState, errorDescription: String?) {
        
        func setUIToNormal() {
            self.viewTitleLabel.text = "Welcome to My Postcard"
            self.emailTextField.placeholder = "Email"
            self.passwordTextField.placeholder = "Password"
            self.loginButton.setTitle("Login", for: .normal)
            self.errorDescriptionLabel.text = ""
            setLoginActivityIndicatorHidden(hidden: true, animated: true)
            self.view.endEditing(true)
        }
        
        func setUIToLogin() {
            self.errorDescriptionLabel.text = ""
            self.view.endEditing(true)
            setLoginActivityIndicatorHidden(hidden: false, animated: true)
        }
        
        func setUIToShowingMessage(_ message: String?) {
            setLoginActivityIndicatorHidden(hidden: true, animated: true)
            guard let text = message else {
                self.errorDescriptionLabel.text = ""
                return
            }
            self.errorDescriptionLabel.text = text
        }
        
        func setLoginActivityIndicatorHidden(hidden: Bool, animated: Bool) {
            func setActivityIndicatorHidden(hidden: Bool) {
                self.loginButton.isEnabled = hidden
            }
            animated ?
                UIView.animate(withDuration: 1) {setActivityIndicatorHidden(hidden: hidden)} : setActivityIndicatorHidden(hidden: hidden)
            
            self.loginButton.setTitle(hidden ? "Login" : "", for: .normal)
            self.loginActivityIndicator.isHidden = hidden
            
            hidden ?
                self.loginActivityIndicator.stopAnimating() :
                self.loginActivityIndicator.startAnimating()
        }
        
        switch state {
        case .normal:
            setUIToNormal()
        case .login:
            setUIToLogin()
        case .showingMessage:
            setUIToShowingMessage(errorDescription)
        }
    }
    
    private func showErrorForType(_ errorType: LoginErrorType) {
        self.setUIFor(state: .showingMessage, errorDescription: errorType.text)
    }
    
}

// MARK: - UI Interaction Control
extension LoginViewController {
    @objc func keyboardWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        guard let keyboardRect = userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? CGRect  else {
            return
        }
        self.scrollView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardRect.height, right: 0.0)
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification) {
        self.scrollView.contentInset = UIEdgeInsets.zero
    }
    
    @objc func backgroundViewTapped() {
        self.view.endEditing(true)
    }
    
    @IBAction func loginButtonHandler(_ sender: UIButton) {
        guard let email = self.emailTextField.text, !email.isEmpty else {
            // missing username
            self.showErrorForType(.usernameIsMissing)
            self.emailTextField.becomeFirstResponder()
            return
        }
        
        guard let password = self.passwordTextField.text, !password.isEmpty else {
            // missing password
            self.showErrorForType(.passwordIsMissing)
            self.passwordTextField.becomeFirstResponder()
            return
        }
        
        self.setUIFor(state: .login, errorDescription: nil)
        
        API.login(email: email, password: password) { loginError in
            guard let error = loginError else {
                self.setUIFor(state: .normal, errorDescription: nil)
                self.performSegue(withIdentifier: "LoginToMain", sender: nil)
                return
            }
            
            self.setUIFor(state: .showingMessage, errorDescription: error.errorMessage)
        }
    }
}

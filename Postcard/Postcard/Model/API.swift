//
//  API.swift
//  Postcard
//
//  Created by Diego Curumaco on 29/04/21.
//

import Foundation
import Alamofire

enum Endpoint: String {
    case login = "/mobile/login.php?json=1"
}

enum Domain: String {
    case mypostcard = "www.mypostcard.com"
}

enum URLProtocol: String {
    case http = "http://"
    case https = "https://"
}

struct URLCreator {
    
    static func urlWith(urlProtocol: URLProtocol, domain: Domain, endpoint: Endpoint) -> String {
        return "\(urlProtocol.rawValue)\(domain.rawValue)\(endpoint.rawValue)"
    }
}

struct API {
    
    static func login(email: String, password: String, completionBlock: @escaping (_ error: APIError?) -> Void) {
        let parameters: Parameters = [
            "email": email,
            "password": password
        ]
        
        let domain: Domain = .mypostcard
        let url = URLCreator.urlWith(urlProtocol: .https, domain: domain, endpoint: .login)
        
        AF.request(url, method: .post, parameters: parameters).responseJSON { response in
            if let error = response.error {
                let apiError = APIError(type: .request, domain: domain, errorMessage: error.localizedDescription)
                completionBlock(apiError)
                return
            }
            
            guard let json = response.value as? [String: Any] else {
                let loginError = APIError(type: .jsonFormat, domain: domain)
                completionBlock(loginError)
                return
            }
            
            if let success = json["success"] as? Bool, success {
                completionBlock(nil)
                return
            }
            
            if let errorMessage = json["errormessage"] as? String {
                let loginError = APIError(type: .unknown, domain: domain, errorMessage: errorMessage)
                completionBlock(loginError)
                return
            }
            
            let loginError = APIError(type: .credentials, domain: domain)
            completionBlock(loginError)
            return
        }
    }
}

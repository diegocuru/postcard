//
//  ImageRepository.swift
//  Postcard
//
//  Created by Diego Curumaco on 6/05/21.
//

import UIKit

struct ImageRepository {
    static func imageName(_ image: UIImage) -> String? {
        let documentsDirectory = URL.documentsDirectory
        let imageName = UUID().uuidString
        let imagePath = documentsDirectory.appendingPathComponent(imageName)
        guard let postcardImageData = image.jpegData(compressionQuality: 0.8) else { return nil }
        
        do {
            try postcardImageData.write(to: imagePath)
            return imageName
        } catch {
            return nil
        }
    }
    
    static func deleteImage(imagePath: String) {
        let url = URL.documentsDirectory.appendingPathComponent(imagePath)
        
        do {
            try FileManager.default.removeItem(at: url)
        } catch {
            print("Could not delete image file")
        }
        
        let fileExist = FileManager.default.fileExists(atPath: url.path)
        if fileExist {
            print("File still existing")
        } else {
            print("File removed successfully")
        }
    }
    
    static func loadImage(from path: String) -> UIImage? {
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        
        if let dirPath = paths.first {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(path)
            if let imageData = NSData(contentsOf: imageURL) {
                return UIImage(data: imageData as Data)
            }
        }
        return nil
    }
}

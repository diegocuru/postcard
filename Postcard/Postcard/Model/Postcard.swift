//
//  Postcard.swift
//  Postcard
//
//  Created by Diego Curumaco on 30/04/21.
//

import Foundation
import UIKit
import CoreData

struct Postcard {
    
    let id: UUID
    let style: PostcardStyle
    let frontText: String
    let backText: String?
    let imagePath: String?
    let lastModified: Date?
    let previewPath: String?
    
    init(id: UUID, style: PostcardStyle, frontText: String, backText: String?, imagePath: String? = nil, lastModified: Date? = nil, previewPath: String? = nil) throws {
        let limit = 30
        
        if !TextValidator.isValidFrontText(frontText, charsLimit: limit) {
            fatalError("Front text should be no longer than \(limit) characters")
        }
        
        if let backText = backText {
            if !TextValidator.isValidBackText(backText, wordsLimit: limit) {
                fatalError("Back text should be no longer than \(limit) words")
            }
        }
        
        self.id = id
        self.style = style
        self.frontText = frontText
        self.backText = backText
        self.imagePath = imagePath
        self.lastModified = lastModified
        self.previewPath = previewPath
    }
    
    init(managedObject: NSManagedObject) throws {
        guard let id = managedObject.value(forKey: "id") as? UUID else {
            fatalError("managed object doesn't contains id")
        }
        self.id = id
        
        guard let style = managedObject.value(forKey: "style") as? Int16 else {
            fatalError("managed object doesn't contains style")
        }
        self.style = style == 0 ? .regular : .foldable
        
        guard let frontText = managedObject.value(forKey: "frontText") as? String else {
            fatalError("managed object doesn't contains front text")
        }
        self.frontText = frontText
        
        if let backText = managedObject.value(forKey: "backText") as? String {
            self.backText = backText
        } else {
            self.backText = nil
        }
        
        guard let imagePath = managedObject.value(forKey: "imagePath") as? String else {
            fatalError("managed object doesn't contains image path")
        }
        self.imagePath = imagePath
        
        guard let lastModified = managedObject.value(forKey: "lastModified") as?  Date else {
            fatalError("managed object doesn't contains last modified")
        }
        self.lastModified = lastModified
        
        guard let previewPath = managedObject.value(forKey: "previewPath") as? String else {
            fatalError("managed object doesn't contains preview path")
        }
        self.previewPath = previewPath
    }
}

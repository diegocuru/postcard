//
//  APIError.swift
//  Postcard
//
//  Created by Diego Curumaco on 29/04/21.
//

import Foundation

enum APIErrorType {
    case request
    case jsonFormat
    case unknown
    case credentials
    
    var code: Int {
        switch self {
        case .request:
            return 9
        case .jsonFormat:
            return 10
        case .unknown:
            return 11
        case .credentials:
            return 12
        }
    }
    
    var description: String {
        switch self {
        case .request:
            return "Bad request"
        case .jsonFormat:
            return "Bad json format"
        case .unknown:
            return "Some error ocurred"
        case .credentials:
            return "Bad credentials"
        }
    }
}

struct APIError {
    let type: APIErrorType
    let domain: Domain
    let errorMessage: String
    
    init(type: APIErrorType, domain: Domain, errorMessage: String? = nil) {
        self.type = type
        self.domain = domain
        self.errorMessage = errorMessage ?? type.description
    }
}

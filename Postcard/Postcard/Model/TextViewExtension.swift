//
//  TextViewExtension.swift
//  Postcard
//
//  Created by Diego Curumaco on 1/05/21.
//

import Foundation
import UIKit

extension UITextView {
    
    func getCharactersCount() -> Int {
        self.text.count
    }
    
    func getWordsCount() -> Int {
        let components = self.text.components(separatedBy: .whitespacesAndNewlines)
        let words = components.filter{( !$0.isEmpty )}
        return words.count
    }
}

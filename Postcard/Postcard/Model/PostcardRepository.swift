//
//  Postcards.swift
//  Postcard
//
//  Created by Diego Curumaco on 2/05/21.
//

import Foundation
import UIKit
import CoreData

enum Entity {
    case postcard
    
    var name: String {
        switch self {
        case .postcard:
            return "PostcardManaged"
        }
    }
}

struct PostcardRepository {
    
    static func getStoredPostcards() -> [Postcard]? {
        guard let managedContext = AppDelegate.managedContext else {
            return nil
        }
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: Entity.postcard.name)
        let sortDescriptor = NSSortDescriptor(key: "lastModified", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        var managedObjects: [NSManagedObject]?
        var postcards: [Postcard]?
        
        do {
            managedObjects = try managedContext.fetch(fetchRequest)
            for managedObject in managedObjects! {
                do {
                    let postcard = try Postcard(managedObject: managedObject)
                    if postcards == nil {
                        postcards = [Postcard]()
                    }
                    postcards?.append(postcard)
                } catch {
                    print("Could not create postcard")
                }
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return nil
        }
        return postcards
    }
    
    static func save(postcard: Postcard, image: UIImage, preview: UIImage, completionBlock:(_ saved: Bool) -> Void) {
        if let imagePath = postcard.imagePath {
            ImageRepository.deleteImage(imagePath: imagePath)
        }
        
        if let previewPath = postcard.previewPath {
            ImageRepository.deleteImage(imagePath: previewPath)
        }
        
        guard let imagePath = ImageRepository.imageName(image), let previewPath = ImageRepository.imageName(preview) else {
            completionBlock(false)
            return
        }
        
        guard let managedContext = AppDelegate.managedContext else {
            completionBlock(false)
            return
        }
        
        guard let entityDescription = NSEntityDescription.entity(forEntityName: Entity.postcard.name, in: managedContext) else {
            completionBlock(false)
            return
        }
        
        let postcardManaged = NSManagedObject(entity: entityDescription, insertInto: managedContext)
        
        postcardManaged.setValue(postcard.id, forKey: "id")
        
        let styleValue = postcard.style == .regular ? 0 : 1
        postcardManaged.setValue(styleValue, forKey: "style")
        
        postcardManaged.setValue(postcard.frontText, forKey: "frontText")
        
        postcardManaged.setValue(imagePath, forKey: "imagePath")
        
        if let backText = postcard.backText {
            postcardManaged.setValue(backText, forKey: "backText")
        }
        if let lastModified = postcard.lastModified {
            postcardManaged.setValue(lastModified, forKey: "lastModified")
        }
        
        postcardManaged.setValue(previewPath, forKey: "previewPath")
        
        do {
            try managedContext.save()
            completionBlock(true)
        } catch let error as NSError {
            print("Could not save \(error). \(error.userInfo)")
            completionBlock(false)
        }
    }
    
    static func delete(postcard: Postcard, completionBlock:(_ deleted: Bool) -> Void) {
        guard let managedContext = AppDelegate.managedContext else {
            completionBlock(false)
            return
        }
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: Entity.postcard.name)
        let predicate = NSPredicate(format: "id == '\(postcard.id)'")
        fetchRequest.predicate = predicate
        
        var managedObjectToDelete: NSManagedObject?
        do {
            managedObjectToDelete = try managedContext.fetch(fetchRequest).first
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        guard let managedObject = managedObjectToDelete else {
            completionBlock(false)
            return
        }
        
        if let imagePath = postcard.imagePath {
            ImageRepository.deleteImage(imagePath: imagePath)
        }
        
        if let previewPath = postcard.imagePath {
            ImageRepository.deleteImage(imagePath: previewPath)
        }
        
        do {
            managedContext.delete(managedObject)
            try managedContext.save()
            completionBlock(true)
        } catch let error as NSError {
            print("Could not delete. \(error), \(error.userInfo)")
            completionBlock(false)
        }
    }
    
    static func postCardExists(id: String) -> Bool {
        guard let managedContext = AppDelegate.managedContext else {
            return false
        }
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: Entity.postcard.name)
        fetchRequest.predicate = NSPredicate(format: "id = %@", id)

        var results: [NSManagedObject] = []

        do {
            results = try managedContext.fetch(fetchRequest)
        }
        catch {
            print("error executing fetch request: \(error)")
        }

        return results.count > 0
    }
    
    static func update(postcard: Postcard, image: UIImage, preview: UIImage, completionBlock:(_ updated: Bool) -> Void) {
        guard let managedContext = AppDelegate.managedContext else {
            completionBlock(false)
            return
        }
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: Entity.postcard.name)
        fetchRequest.predicate = NSPredicate(format: "id = %@", postcard.id.uuidString)
        
        do {
            let managedObjects = try managedContext.fetch(fetchRequest)
            
            guard let postcardManaged = managedObjects.first else {
                completionBlock(false)
                return
            }
            
            if let imagePath = postcard.imagePath {
                ImageRepository.deleteImage(imagePath: imagePath)
            }
            
            if let previewPath = postcard.previewPath {
                ImageRepository.deleteImage(imagePath: previewPath)
            }
            
            guard let imagePath = ImageRepository.imageName(image), let previewPath = ImageRepository.imageName(preview) else {
                completionBlock(false)
                return
            }
            
            guard let managedContext = AppDelegate.managedContext else {
                completionBlock(false)
                return
            }
            
            let styleValue = postcard.style == .regular ? 0 : 1
            postcardManaged.setValue(styleValue, forKey: "style")
            postcardManaged.setValue(postcard.frontText, forKey: "frontText")
            postcardManaged.setValue(imagePath, forKey: "imagePath")
            
            if let backText = postcard.backText {
                postcardManaged.setValue(backText, forKey: "backText")
            }
            
            if let lastModified = postcard.lastModified {
                postcardManaged.setValue(lastModified, forKey: "lastModified")
            }
            postcardManaged.setValue(previewPath, forKey: "previewPath")
            
            do {
                try managedContext.save()
                completionBlock(true)
            } catch let error as NSError {
                print("Could not save \(error). \(error.userInfo)")
                completionBlock(false)
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            completionBlock(false)
            return
        }
    }
}

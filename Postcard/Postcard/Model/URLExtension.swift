//
//  URLExtension.swift
//  Postcard
//
//  Created by Diego Curumaco on 6/05/21.
//

import Foundation

extension URL {
    static var documentsDirectory: URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
}

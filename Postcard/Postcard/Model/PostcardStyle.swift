//
//  PostcardStyle.swift
//  Postcard
//
//  Created by Diego Curumaco on 2/05/21.
//

import Foundation

enum PostcardStyle {
    case regular
    case foldable
    
    var name: String {
        switch self {
        case .regular:
            return "regular"
        case .foldable:
            return "foldable"
        }
    }
}

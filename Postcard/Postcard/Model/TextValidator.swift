//
//  TextValidator.swift
//  Postcard
//
//  Created by Diego Curumaco on 30/04/21.
//

import Foundation

struct TextValidator {
    static func isValidFrontText(_ text: String, charsLimit: Int) -> Bool {
        return text.count <= charsLimit
    }
    
    static func isValidBackText(_ text: String, wordsLimit: Int) -> Bool {
        let components = text.components(separatedBy: .whitespacesAndNewlines)
        let words = components.filter{( !$0.isEmpty )}
        return words.count <= wordsLimit
    }
}
